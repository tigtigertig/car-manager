﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarManager {
    public partial class SelectYearForm : Form {
        public int yearFrom;
        public int yearTo;

        // Constructor
        public SelectYearForm(List<Car> cars) {
            InitializeComponent();
            // Creates a new List<> of Car objects to hold a copy of the past list, sorted by year.
            List<Car> sortedCars = new List<Car>();
            sortedCars = cars.OrderBy(x => x.Year).ToList();

            // Iterates through the sorted list and adds the year to both CombinationBox's if not already added.
            foreach(Car c in sortedCars) {
                if(!combFrom.Items.Contains(c.Year)) combFrom.Items.Add(c.Year);
                if(!combTo.Items.Contains(c.Year)) combTo.Items.Add(c.Year);
            }
        }

        // Handles btnOK's click event.
        private void btnOK_Click(object sender, EventArgs e) {
            // If either box has no selection, return.
            if (combFrom.SelectedIndex == -1 || combTo.SelectedIndex == -1) return;

            // Converts the selections into an integer.
            yearFrom = Convert.ToInt32(combFrom.SelectedItem.ToString());
            yearTo = Convert.ToInt32(combTo.SelectedItem.ToString());

            // If the year from is larger than the year to, display an error.
            if (yearFrom > yearTo) {
                MessageBox.Show("Please specify a valid range.", "Input Error");
                return;
            }

            // Sets the dialog result to OK so the MainForm knows a valid selection has been made.
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
